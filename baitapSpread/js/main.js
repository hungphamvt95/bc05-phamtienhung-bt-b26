let headingText = document.querySelector(".heading").innerHTML;
let headingChars = [...headingText];

var contentHTML = "";
headingChars.forEach((item) => {
  contentHTML += `
    <span>${item}</span>
    `;
});
document.querySelector(".heading").innerHTML = contentHTML;
