function tinhDiemTB(...diem) {
  let diemTB = 0;
  diem.forEach((item) => {
    diemTB += item;
    console.log("diemTB1: ", diemTB);
  });
  diemTB = diemTB / diem.length;
  console.log("diemTB: ", diemTB);
  return diemTB.toFixed(2);
}

document.getElementById("btnKhoi1").addEventListener("click", () => {
  var diemToanK1 = document.getElementById("inpToan").value * 1;
  var diemLyK1 = document.getElementById("inpLy").value * 1;
  var diemHoaK1 = document.getElementById("inpHoa").value * 1;
  var diemTBK1 = tinhDiemTB(diemToanK1, diemLyK1, diemHoaK1);
  document.getElementById("tbKhoi1").innerHTML = diemTBK1;
});
document.getElementById("btnKhoi2").addEventListener("click", () => {
  var diemVanK2 = document.getElementById("inpVan").value * 1;
  var diemSuK2 = document.getElementById("inpSu").value * 1;
  var diemDiaK2 = document.getElementById("inpDia").value * 1;
  var diemEnglishK2 = document.getElementById("inpEnglish").value * 1;
  var diemTBK2 = tinhDiemTB(diemVanK2, diemSuK2, diemDiaK2, diemEnglishK2);
  document.getElementById("tbKhoi2").innerHTML = diemTBK2;
});
