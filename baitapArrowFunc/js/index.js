const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
let loadButton = () => {
  var contentHTML = "";
  colorList.forEach((color) => {
    contentHTML += `
    <button id="${color}" class ="color-button ${color}"></button>
    `;
  });
  document.getElementById("colorContainer").innerHTML = contentHTML;
};
loadButton();

let changeColor = () => {
  var btn = document.getElementsByClassName("color-button");
  for (let i = 0; i < btn.length; i++) {
    btn[i].addEventListener("click", function () {
      var current = document.getElementsByClassName("active");
      console.log("current: ", current);

      if (current.length > 0) {
        console.log("current.length : ", current.length);
        current[0].className = current[0].className.replace(" active", "");
      }

      this.className += " active";
      document.getElementById("house").className = `house ${this.id}`;
    });
  }
};
changeColor();
